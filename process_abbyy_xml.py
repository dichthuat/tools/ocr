#!/usr/bin/env python
#
# 2019.04.04 Dang Doan
# Licensed under the MIT license
"""
Process the XML file that was the output of ABBYY FineReader Engine 11
Test with interactive Python:
from process_abbyy_xml import *
file = 'test/abbyy_cloud_ocr_514_Dien_from_Python.xml'
ET_object = clean_xml_read(file)
doc_data = parse_abbyy_document(ET_object)
doc_data.to_json_file('test_new.json')
"""

import argparse
import xml.etree.ElementTree as ET

from OcrData import OCR_Data

def join_lines(*lines):
    return ' '.join(lines)

def join_paragraphs(*paras):
    """Use line break of DOS/Windows type: CR LF == \r\n
    """
    return '\r\n'.join(paras)

def clean_xml_read(xml_file):
    """
    Parse an XML file and remove namespaces inside it
    """
    with open(xml_file) as file:
        xml_string = file.read()
    xml_string = strip_namespace_xml(xml_string)
    ET_tree = parse_xml_text(xml_string)
    return ET_tree

def strip_namespace_xml(xml_string):
    import re
    xml_string = re.sub(' xmlns="[^"]+"', '', xml_string, count=1)
    return xml_string

def parse_xml_text(xml_string):
    """
    Parse an XML text using ElementTree
    """
    root = ET.fromstring(xml_string)
    return root

def parse_abbyy_document(ET_element_doc):
    """
    Parse text from an object named document from ABBYY XML file
    """
    doc_text = ''
    doc_data = OCR_Data()
    for page in ET_element_doc.iter('page'):
        page_data = parse_abbyy_page(page)
        doc_text = join_paragraphs(doc_text, page_data.text)
        doc_data.append(page_data)
    confidence = 1
    box = (-1, -1, -1, -1) #document element in ABBYY XML has no bounding box info
    doc_data.text = doc_text
    doc_data.confidence = confidence
    doc_data.box = box
    doc_data.tag = 'document'
    #doc_data.show()
    return doc_data

def parse_abbyy_page(ET_element_page):
    """
    Parse text from an object named page from ABBYY XML file
    """
    page_text = ''
    page_data = OCR_Data()
    for block in ET_element_page.iter('block'):
        # Only take into account following block types:
        # blockType="Table", blockType="Text"
        if block.attrib['blockType'] in {'Text', 'Table'}:
            block_data = parse_abbyy_block(block)
            page_text = join_paragraphs(page_text, block_data.text)
            page_data.append(block_data)
    confidence = 1
    box = (-1, -1, -1, -1) #page element in ABBYY XML has no bounding box info
    page_data.text = page_text
    page_data.confidence = confidence
    page_data.box = box
    page_data.tag = 'page'
    page_data.attrib = ET_element_page.attrib # page in ABBYY XML has attributes to be used:
    # {'originalCoords': '1', 'height': '2832', 'resolution': '300', 'width': '2006'}
    #page_data.show()
    return page_data


def parse_abbyy_block(ET_element_block):
    """
    Parse text from an object named block from ABBYY XML file
    """
    block_text = ''
    block_data = OCR_Data()
    for para in ET_element_block.iter('par'):
        para_data = parse_abbyy_paragraph(para)
        block_text = join_paragraphs(block_text, para_data.text)
        block_data.append(para_data)
    confidence = 1
    box = (int(ET_element_block.attrib['l']), int(ET_element_block.attrib['t']),
           int(ET_element_block.attrib['r']), int(ET_element_block.attrib['b']))
    block_data.text = block_text
    block_data.confidence = confidence
    block_data.box = box
    block_data.tag = 'block'
    #block_data.show()
    return block_data

def parse_abbyy_paragraph(ET_element_par):
    """
    Parse text from an object named par from ABBYY XML file
    """
    para_text = ''
    para_data = OCR_Data()
    for line in ET_element_par.iter('line'):
        line_data = parse_abbyy_line(line)
        para_text = join_lines(para_text, line_data.text)
        para_data.append(line_data)
    confidence = 1
    box = (-1, -1, -1, -1) #par element in ABBYY XML has no bounding box info
    para_data.text = para_text
    para_data.confidence = confidence
    para_data.box = box
    para_data.tag = 'paragraph'
    #para_data.show()
    return para_data

def parse_abbyy_line(ET_element_line):
    """
    Parse text from an object named line from ABBYY XML file
    """
    line_text = ''
    for characters in ET_element_line.iter('charParams'):
        if isinstance(characters.text, str):
            line_text += characters.text
        # fixed bug depending on input (without if): line_text += characters.text
        # TypeError: Can't convert 'NoneType' object to str implicitly
        # example:
        # <charParams l="1134" t="1989" r="1304" b="2027"></charParams>
    confidence = 1
    box = (int(ET_element_line.attrib['l']), int(ET_element_line.attrib['t']),
           int(ET_element_line.attrib['r']), int(ET_element_line.attrib['b']))
    line_data = OCR_Data(line_text, confidence, box, 'line')
    return line_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('detect_file', help='The XML file from ABBYY Cloud OCR.')
    parser.add_argument('-json_file', help='Optional output json file', default=0)
    args = parser.parse_args()
    
    ET_object = clean_xml_read(args.detect_file)
    doc_data = parse_abbyy_document(ET_object)
    if args.json_file != 0:
        doc_data.to_json_file(args.json_file)
    print(doc_data)
