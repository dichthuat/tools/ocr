#!/usr/bin/env python

from process_abbyy_xml import *
pdf_file1 = 'demo/Dien_511-513_Duc.pdf'
file1 = 'demo/Dien_511-513_Duc_XCA_Extended.xml'
pdf_file2 = 'demo/Dien_511-513_Viet.pdf'
file2 = 'demo/Dien_511-513_Viet_offline.xml'

output_csv = 'demo/export_Dien_align.csv'

ET_object1 = clean_xml_read(file1)
doc_data1 = parse_abbyy_document(ET_object1)
ET_object2 = clean_xml_read(file2)
doc_data2 = parse_abbyy_document(ET_object2)
from align_pages import *

text_doc_1, text_doc_2 = align_pair_doc_to_csv(doc_data1, doc_data2, output_csv)


from pdf2image import convert_from_path

pages_pdf1 = convert_from_path(pdf_file1, 275)

for page in pages_pdf1:
    page_no = pages_pdf1.index(page)
    page_image = "%s-page%d.png" % (pdf_file1, page_no)
    page.save(page_image, "PNG")
    #page_image_with_bounds = "%s-page%d_done.png" % (pdf_file1, page_no)
    render_doc_text(page_image, doc_data1[page_no], page_image, 0)

pages_pdf2 = convert_from_path(pdf_file2, 275)

for page in pages_pdf2:
    page_no = pages_pdf2.index(page)
    page_image = "%s-page%d.png" % (pdf_file2, page_no)
    page.save(page_image, "PNG")
    #page_image_with_bounds = "%s-page%d_done.png" % (pdf_file2, page_no)
    render_doc_text(page_image, doc_data2[page_no], page_image, 0)

#aligner_scripts_dir = 'aligner/scripts'
#ALIGNER_DIR = 'aligner'
#import Auto_Align_Compare

#Auto_Align_Compare.auto_align_file_list('list_align_Duc.txt', 'list_align_Duc.txt', ALIGNER_DIR+'/LF_aligner_3.11.sh', 'de', 'vi')
