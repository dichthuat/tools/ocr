Unterscheidung nach Art der Signalverarbeitung
 Steuerungen teilt man nach der Art der Signalver¬ arbeitung ein in:
 • Verknüpfungssteuerung und
 • Ablaufsteuerung (Bild 1).
 Verknüpfungssteuerung
 Sind bei einer Steuerung Eingangssignale von mehreren Signalgebern vorhanden, so müssen diese Signale zusammengeführt und unter Berück¬ sichtigung logischer Verknüpfungen zu einem Ausgangssignal verarbeitet werden. Solche Steue¬ rungen werden auch als kombinatorische Steue¬ rungen bezeichnet. Sie beinhalten weder Zeit- noch Speicherverhalten. Reine Verknüpfungssteuerun¬ gen sind selten. Häufig ist eine Verknüpfungssteue¬ rung (Bild 2) Bestandteil einer anderen Steuerung, z.B. einer Ablaufsteuerung. Verknüpfungssteue¬ rungen werden angewendet z.B. bei Schutz- und Sicherheitseinrichtungen von Maschinen oder beim Auswerten von Betriebszuständen.
 Ablaufsteuerung
 Eine Ablaufsteuerung (Seite 530) arbeitet in einzelnen Schritten, die in einer vorgegebenen Folge ablaufen. Sie geht nur dann einen Schritt weiter, wenn die Weiterschaltbedingungen für diesen Schritt erfüllt sind. Sind die Weiterschaltbedingungen von der Zeit abhängig, d.h., werden sie mit elektronischen Zeitgliedern, z. B. Taktgeber oder Zeitrelais, erzeugt, so spricht man von einer zeitgeführten Ablaufsteuerung.
 Bei der prozessgeführten Ablaufsteuerung liefert der gesteuerte Prozess die Weiterschaltbedingungen. Dies führt dazu, dass ein geschlossener Wirkungsablauf entsteht. Dabei handelt essich definitionsgemäß trotzdem um eine Steuerung, da rückgeführte Größen nicht im Sinne einer Anpassung an die Führungs¬ größe, wie bei der Regelung, verwendet werden.
 Unterscheidung nach Art der Programmverwirklichung
 Als Programm einer Steuerung bezeichnet man sämtliche Anweisungen und Vereinbarungen, die eine zu steuernde Anlage je nach Aufgabe beeinflussen. Dieses Programm lässt sich durch unterschiedliche Funktionsglieder und deren Verbindungen oder als eine Abfolge von Steueranweisungen, die in Spei¬ chern hinterlegt sind, realisieren. Steuerungen kann man hinsichtlich der Programmverwirklichung in verbindungsprogrammierte und in speicherprogrammierte Steuerungen unterteilen (Tabelle).

 Tabelle: Programmverwirklichung
 Art
 Beispiel
 Verbindungs¬
 programmiert
 (VPS)
 fest programmiert
 Programm wird durch die Wahl der Bauelemente und deren Verbindung festgelegt, z. B. Schütz- oder Relaissteuerungen.
 umprogrammierbar
 Programmsteuerung, z.B. über Steckverbinder, Diodenmafrix.
 Speicher¬
 programmiert
 (SPS)
 (Seite 516)
 austauschprogrammierbar
 Programmspeicher ist ein Nur-Lese-Speicher (ROM). Programm¬ änderung nur durch Auswechseln des ROM.
 frei programmierbar
 Programmspeicher ist ein Schreib-Lese-Speicher (RAM). Pro¬ grammeingabe erfolg! überein Programmiergerät oder PC.

 1 Was versteht man unter Steuern? 4 Erläutern Sie die Begriffe a) Führungsgröße,
 2 Worin besteht der wesentliche Unterschied b) Stellgröße, c) Störgröße und d) Stellglied,
 zwischen einer Steuerung und einer Regelung? 5 Nach welchen Merkmalen lassen sich Steuerungen
 3 Skizzieren Sie den Wirkungsplan einer Steuerung unterscheiden?

 Bild 1: Einteilung von Steuerungen nach Art der Signalverarbeitung

 Heizung erst dann „EIN"", wenn Thermostat und Lüfter „EIN""
