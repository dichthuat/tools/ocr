15.1 Kỹ thuật điều khiển
15.1.1 Điều khiển
Trong kỹ thuật, những đại luợng như điện áp, cuờng độ dòng điện, vận tốc quay hay nhiệt độ đổu đuợc điếu khiển hay diều chỉnh.
Việc suời ẩm căn phòng với lò sưởi (Hình 1) là thí dụ sẽ cho thấy sự khác biệt giữa điều khiển và điều chỉnh.
Điều khiển một lò sưởi (Hình 1) có nghĩa là điếu khiển vị trí đóng mở của van tùy thuộc vào trị số của nhiệt độ ở bên ngoài (đại luợng chuẩn hay biến chuẩn w), đuợc đo bởi đầu dò (bộ cảm biến).
Nhược dlổm của sự điều khiển này, thí dụ như vỉ khồng nhận ra sự giảm nhiệt độ trong phòng cho nên nó không ảnh huởng, tác động lên lò sưởi.
Một quy trình được gọi là điều khiển khi đại lượng ở đầu vào tạo ảnh hưởng ở đầu ra theo một quy luật định sẵn.
Đại lượng ở đầu ra không có tác dụng ngược trở lạl vào đầu vào (trình tự tác động mở), có nghía là qua đó tác dụng của đại luợng gây nhiễu không được bù trừ.
Trong Hlnh 1 lò sưởi bị nhiễu, thí dụ như nhiệt dộ trong phòng giảm xuống do của sổ mở mà khống được cân bằng.
Nguợc lạl diều chỉnh có nghĩa là đo nhiệt dộ trong phòng và so sánh nó VỚI một trị số định múc mong muốn, thí dụ như 20 °c.
Tùy theo sự chênh lệch này sẽ hình thành dạl lượng (biến) điếu khiển, gây ảnh huởng việc đóng mở van điều chỉnh dòng nhlột (dầu, hơi nước...) chảy vào.
Khác với diều khiển, quá trinh điều chỉnh nhận ra sự giảm nhiệt dộ trong phòng và cân bằng qua việc gla tăng dòng nhiệt chảy vào lò suởl.
Để cảl thiện việc điếu hòa nhiệt độ, ngoài việc đo nhiệt độ bên ngoài còn kèm thêm đo nhiột độ trong phòng.
15.1.1.1 Thuật ngữ về hệ thống điều khiển
So đổ tác động (Hlnh 2) cho thấy rố tác dụng hỗ tương giũa những thảnh phán tham gla trong h$ thống dléu khiển.
Những mũl tốn cho biết hướng tác động của tín hiệu dlâu khiển, thí dụ như hướng truyền năng luợng hay lưu luụng dòng chảy.
Đạl lượng (biến) chuẩn w là dạl lượng ở dẩu vào, thí dụ như nhiệt dộ bên ngoài, cố kỷ hiệu là w.
Đạl lượng chuẩn đuọc đo từ thiết bị điều khiển qua bộ phát tín hiệu (thi dụ như bộ cảm biển), đo nhiệt độ ở bên ngoài.
