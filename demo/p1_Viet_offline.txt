 15.1 Kỹ thuật điều khiển
 15.1.1 Điêu khiển
 Trong kỹ thuật, những đại lượng như điện áp, cường độ dòng điện, vận tốc quay hay nhiệt độ đều đuợc điều khiển hay điều chỉnh. Việc sưởi ấm căn phòng với lò sưởi (Hình 1) là thí dụ sẽ cho thấy sự khác biệt giữa điều khiển và điếu chỉnh.
 Điều khiển một lò suởi (Hình 1) có nghĩa là điều khiển vị trí đóng mở của van tùy thuộc vào trị số của nhiệt độ ở bên ngoài (đại luợng chuẩn hay biến chuẩn w), được đo bởi đầu dò (bộ cảm biến). Nhược điểm của sự điều khiển này, thí dụ như vì không nhận ra sự giảm nhiệt độ trong phòng cho nên nó không ảnh huởng, tác động lên lò sưởi.
 Một quy trình đuọc gọi là điều khiển khi đại lượng ở đầu vào tạo ảnh hưởng ở đầu ra theo một quy luật định sẵn. Đại lượng ở đáu ra không có tác dụng nguọc trở lại vào đầu vào (trình tự tác động mở), có nghĩa là qua đó tác dụng của đại luợng gây nhiễu không đưọc bù trừ.

 Trong Hlnh 1 lò sưởi bị nhiễu, thí dụ như nhiệt độ trong phòng giảm xuống do cửa sổ mở mà không được cân bằng.
 Nguọc lại điều chỉnh có nghĩa là đo nhiệt độ trong phòng và so sánh nó với một trị số định múc mong muốn, thí dụ như 20 °c. Tùy theo sự chênh lệch này sẽ hình thành đại lượng (biến) điều khiển, gây ảnh huởng việc đóng mở van điều chỉnh dòng nhiệt (dẩu, hơi nước...) chảy vào. Khác với điều khiển, quá trình điều chỉnh nhận ra sự giảm nhiệt độ trong phòng và cân bằng qua việc gia tăng dòng nhiệt chảy vào lò suởi. Để cải thiện việc điều hòa nhiệt độ, ngoài việc đo nhiệt độ bên ngoài còn kèm thêm đo nhiệt độ trong phòng.
 Sơ đổ tác động (Hình 2) cho thấy rõ tác dụng hỗ tương giữa những thành phẩn tham gia trong hệ thống điều khiển. Những mũi tên cho biết hướng tác động của tín hiệu điều khiển, thí dụ như hướng truyền năng luợng hay lưu lượng dòng chảy.
 Đại lượng (biến) chuẩn w là đại lượng ở đầu vào, thí dụ như nhiệt độ bên ngoài, có kỷ hiệu là w. Đại lượng chuẩn đuọc đo từ thiết bị điều khiển qua bộ phát tín hiệu (thí dụ như bộ cảm biển), đo nhiệt độ ở bên ngoài.

 Đléu khiổn và đléu chinh

 Điều khiển là sự tác động vào đạl lượng đầu ra do một hay nhiều đại lượng ờ đáu vào theo một quy luật có sẵn. Dấu hiệu đặc trung cho h$ thống điều khiển này là quy trình tác động mở (Hlnh 2).

 Điều chinh là quá trình ở đó đạl lượng điều chinh liên tục đưọc đo vã so sánh vói đại lượng chuẩn. Kết quả là có sự thích ứng liên tục vói đại lượng chuẩn. Khác với điều khiển, điều chỉnh là một quy trình tác động kín, trong đó chính đại lượng điều chinh tự nó có ảnh hưởng liên tục.


 M

 ỵ Đạl lượng điếu khiển X Đạl lượng điều chình w Đại lượng chuần

 Nhiệt độ bên ngoài

 Hình 1: Điều khiển và điổu chỉnh một lò suởi


 ỨR Nhiệt độ ở trong phòng w Đại lượng chuẩn (thí dụ 20 °C)


 Đại luụng điều chinh Xa(nhiệt độ trong phòng Ỗr)


 Quá trình tác động mở (khổng có tác động ngược vào đại lượng chuẩn vv)
