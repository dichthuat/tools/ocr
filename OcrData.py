#!/usr/bin/env python
#
# 2019.04.04 Dang Doan
# Licensed under the MIT license
"""
Class OCR_Data for storing OCR elements, each object has the following info:
+ text: its text
+ confidence: the confidence number for the text identified by OCR, 1 is absolute
+ box: the position of the text in its current page, (left, top, right, bottom)
+ tag: the additional label of the OCR element, often be used to specify type of text, e.g. page, block
+ children: accessed with index, like: object[0] is the first child, each child is also an OCR_Data object.
"""

class OCR_Data:
    """Class for storing data of OCR elements.
    Each item of OCR element contains 3 values: text, confidence, box.
    *text* is a string, or None.
    *confidence* is a decimal number from 0 to 1, or None.
    *box* is a tuple with 4 numbers: (left, top, right, bottom), or None.
    *tag* is a string, or None. This is the name of type of the item, e.g. page, block
    *attrib* contains additional attributes of the element
    """
    text_default = None
    confidence_default = None
    box_default = None
    tag_default = ''
    attrib_default = None

    def __init__(self, text = text_default, confidence = confidence_default, box = box_default, tag = tag_default, attrib = attrib_default):
        self.text = text
        self.confidence = confidence
        self.box = box # = (left, top, right, bottom)
        self.tag = tag
        self.attrib = attrib
        self._children = []

    def show(self):
        print(self)

    def __str__(self):
        #print(' text: {}\n confidence: {}\n position: {}'.format(self.text, self.confidence, self.box))
        return 'Object %s:\n text: %s\n confidence: %s\n position: %s\n additional attributes: %s' % (self.tag, self.text, self.confidence, self.box, self.attrib)

    def get_dict(self):
        return {'text':self.text, 'confidence':self.confidence, 'box':self.box, 'tag':self.tag, 'attrib':self.attrib}

    def __repr__(self):
        return "<%s %r at %#x>" % (self.__class__.__name__, self.tag, id(self))

    def makeelement(self, text, confidence, box, tag, attrib):
        """Create a new element with the same type.
        *text* is a string, or none.
        *confidence* is a decimal number from 0 to 1, or none.
        *box* is a tuple with 4 numbers: (left, top, right, bottom).
        *tag* is a string, or None. This is the name of type of the item, e.g. page, block
        """
        return self.__class__(text, confidence, box, tag, attrib)
    def copy(self):
        """Return copy of current element.
        This creates a shallow copy. Subelements will be shared with the
        original tree.
        """
        elem = self.makeelement(self.text, self.confidence, self.box, self.tag, self.attrib)
        elem.text = self.text
        elem.confidence = self.confidence
        elem.box = self.box
        elem.tag = self.tag
        elem.attrib = self.attrib
        elem[:] = self
        return elem

    def __len__(self):
        return len(self._children)    

    def __getitem__(self, index):
        return self._children[index]

    def __setitem__(self, index, element):
        if isinstance(index, slice):
            for elem in element:
                self._assert_is_element(elem)
        else:
            self._assert_is_element(element)
        self._children[index] = element

    def __delitem__(self, index):
        del self._children[index]

    def append(self, subelement):
        """Add *subelement* to the end of this element.
        The new element will appear in document order after the last existing
        subelement (or directly after the text, if it's the first subelement),
        but before the end tag for this element.
        """
        self._assert_is_element(subelement)
        self._children.append(subelement)

    def extend(self, elements):
        """Append subelements from a sequence.
        *elements* is a sequence with zero or more elements.
        """
        for element in elements:
            self._assert_is_element(element)
        self._children.extend(elements)

    def insert(self, index, subelement):
        """Insert *subelement* at position *index*."""
        self._assert_is_element(subelement)
        self._children.insert(index, subelement)

    def _assert_is_element(self, e):
        """Assert an element of the type OCR_Data
        """
        if not isinstance(e, OCR_Data):
            raise TypeError('expected an OCR_Data, not %s' % type(e).__name__)

    def remove(self, subelement):
        """Remove matching subelement.
        This method compares elements based on
        identity, NOT ON tag value or contents.  To remove subelements by
        other means, the easiest way is to use a list comprehension to
        select what elements to keep, and then use slice assignment to update
        the parent element.
        ValueError is raised if a matching element could not be found.
        """
        # assert iselement(element)
        self._children.remove(subelement)

    def clear(self):
        """Reset element.
        This function removes all subelements, set all attributes to None, clear the tag.
        """
        self.text = self.confidence = None
        self.box = None
        self.tag = ''
        self.attrib = None
        self._children = []

    def to_json_list(self):
        """Recursively loop through the tree to pick data and convert it into json.
        Note: there could be cyclic links, e.g. B is child of A, A is child of B,
        this 'orbital' situation should be broken. TODO
        """
        node_json = {'text': self.text, 'confidence': self.confidence, 'box': self.box, 'attrib': self.attrib}
        for child in self._children:
            if not child.tag in node_json:
                node_json[child.tag] = []
            node_json[child.tag].append(child.to_json_list())
        return node_json

    def to_json_string(self):
        """Output data structure of the whole tree into a JSON string
        """
        import json
        return json.dumps(self.to_json_list(), indent=4, ensure_ascii=False)

    def to_json_file(self, filename):
        """Output data structure of the whole tree into a JSON file
        """
        import json
        with open(filename, 'wt', encoding='utf8') as jsonfile:
            json.dump(self.to_json_list(), jsonfile, indent=4, ensure_ascii=False)

    def iter(self, tag=None):
        """Create tree iterator.
        The iterator loops over the element and all subelements in document
        order, returning all elements with a matching tag.
        If the tree structure is modified during iteration, new or removed
        elements may or may not be included.  To get a stable set, use the
        list() function on the iterator, and loop over the resulting list.
        *tag* is what tags to look for (default is to return all elements)
        Return an iterator containing all the matching elements.
        """
        if tag == "*":
            tag = None
        if tag is None or self.tag == tag:
            yield self
        for e in self._children:
            yield from e.iter(tag)

    def iter_text(self):
        """Create text iterator.
        The iterator loops over the element and all subelements in document
        order, returning all inner text.
        """
        tag = self.tag
        if not isinstance(tag, str) and tag is not None:
            return
        t = self.text
        if t:
            yield t
        for e in self:
            yield from e.itertext()

    def iter_child_text(self):
        """Create text iterator.
        The iterator loops over the children of the element, returning their inner texts.
        """
        tag = self.tag
        if not isinstance(tag, str) and tag is not None:
            return
        for t in self._children:
            if t.text:
                yield t.text
        #for e in self:
            #yield from e.text
