# 2019-03-22 Dang
# python3 picture_write_text.py picture.jpg

import sys, os
from PIL import Image, ImageDraw, ImageFont

def saveImageText(text, image_file):
    image = textDraw(text)
    image.save(image_file)
    
def textDraw(text, width = 100, height = 200, x_pos = 0, y_pos = 0, fontcolor = (0,0,0), bgcolor = (255,255,255)):
	'''Draw a text as a picture'''
	image = Image.new('RGB',(width,height),bgcolor)
	font = ImageFont.truetype('FreeSans.ttf',30)
	draw = ImageDraw.Draw(image)
	draw.text((x_pos,y_pos), text, font=font, fill=fontcolor)
	return image

if __name__ == '__main__':
    filein = str(sys.argv[1])
    image = Image.open(filein)
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype("FreeSans.ttf", 15)
    draw.text((100, 100), "hello tiếng Việt", font=font)

    if len(sys.argv)>2:
        exportFile = str(sys.argv[2])
    else:
        mainname, extname = os.path.splitext(filein) # mainname is '123', extname is '.jpg'
        exportFile = mainname + '_add_text_' + '.png'

    image.save(exportFile)
