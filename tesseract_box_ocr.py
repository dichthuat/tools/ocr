#!/usr/bin/env python
#
# 2019.03.27 Dang Doan
# Licensed under the MIT license
"""
Use tesseract to recognize text in a box (left, top, right, bottom)
python3 tesseract_box_ocr.py 514_Dien_v27_vi-1.png -l vie -box "(448, 664, 2144, 1304)"
"""

# import the necessary packages
import pytesseract
from PIL import Image
import argparse
from ast import literal_eval as make_tuple

from OcrData import OCR_Data

def tesseract_box_ocr(image_file, box=(10, 10, 50, 30), language='eng'):
    """box contains coordinate: (left, top, right, bottom)"""

    image = Image.open(image_file)
    sub_image = image.crop(box)
    
    config = ("-l " + language + " --oem 1")
    text = pytesseract.image_to_string(sub_image, config=config)
    
    OCR_item = OCR_Data()
    OCR_item.text = text
    OCR_item.box = box
    return OCR_item

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('detect_file', help='The image for text detection.')
    parser.add_argument('-box', help='Optional box within image to be detected', default="(10, 10, 50, 30)")
    parser.add_argument('-l', help='Optional language of the text', default='eng')
    parser.add_argument('-out_file', help='Optional output image file', default=0)
    parser.add_argument('-json_file', help='Optional output json file', default=0)
    args = parser.parse_args()

    box_ocr = tesseract_box_ocr(args.detect_file, make_tuple(args.box), args.l)
    print(' Text: {}'.format(box_ocr.text))
    print(' Box position: {}'.format(box_ocr.box))
