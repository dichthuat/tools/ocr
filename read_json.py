#!/usr/bin/env python

# Dang Doan, 2019.03.21
# Read json file containing the result of Google Cloud Vision, parsed by Dang's doctext_detect.py
# Extract the full text and save to a text file

import json
from pprint import pprint
import sys, os

inputFile = str(sys.argv[1])

if len(sys.argv)>2:
    exportFile = str(sys.argv[2])
else:
    mainname, extname = os.path.splitext(inputFile) # mainname is '123', extname is '.json'
    exportFile = mainname + '.txt'

with open(inputFile, 'rt') as f:
    data = json.load(f)

with open(exportFile, 'wt') as text_file:
    text_file.write(data['page'][0]['text'])

#pprint(data)

print(data['page'][0]['text'])
#print(data['page'][0]['block'][0]['text'])
#print(data['page'][0]['block'][0]['paragraph'][0]['text'])
#print(data['page'][0]['block'][0]['paragraph'][0]['word'][1]['text'])
