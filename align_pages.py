#!/usr/bin/env python
#
# 2019.04.04 Dang Doan
# Licensed under the MIT license
"""
Align the boxes from OCR result on a page, assuming the column structure is known.
Test script:

from process_abbyy_xml import *
file = 'test/abbyy_cloud_ocr_514_Dien_from_Python.xml'
ET_object = clean_xml_read(file)
doc_data = parse_abbyy_document(ET_object)
from align_pages import *

#bounds_block = [block.box for block in doc_data.iter('block')]

##sorted_boxes = columnize_boxes(bounds_block, 4082, 5827, 2)
##for index in sorted_boxes[0]: print(bounds_block[index])
##for index in sorted_boxes[1]: print(bounds_block[index])

#ordered_boxes, _ = order_boxes(bounds_block, 4082, 5827, 2)
#for box in ordered_boxes[0]: print(box)
#for box in ordered_boxes[1]: print(box)

#blocks = [block for block in doc_data.iter('block')]
#ordered_blocks, _ = order_blocks(blocks,  4082, 5827, 2)
#for box in ordered_blocks[0]:
   #print(box.box)
   #print(box.text)

ordered_page = page_order(doc_data[0], 4082, 5827, 2)

#for box in ordered_page:
  #print(box.box)
  #print(box.text)

#for box in doc_data[0]:
  #print(box.box)
  #print(box.text)

blocks1 = ordered_page[:]
newblocks = ordered_page[:].copy()

filtered_blocks1 = remove_short_pieces(blocks1,100)
filtered_blocks2  = remove_short_pieces(newblocks,100)
#matching_series = compare_blocks(filtered_blocks1, filtered_blocks2)

# create new modified data and compare
new_filtered_blocks = [block.copy() for block in filtered_blocks1]
for block in new_filtered_blocks:
    block.box = (block.box[0]+50, block.box[1]+50, block.box[2]+50, block.box[3]+50)

matching_series = compare_blocks(filtered_blocks1, new_filtered_blocks)

ocr_page2 = ordered_page.copy()
align_sorted_page(ordered_page, ocr_page2, 'export_Dien_align.csv')

---
Real align:

from process_abbyy_xml import *
file1 = 'test/Dien_511_Duc.xml'
file2 = 'test/Dien_511_Viet.xml'
ET_object1 = clean_xml_read(file1)
doc_data1 = parse_abbyy_document(ET_object1)
ET_object2 = clean_xml_read(file2)
doc_data2 = parse_abbyy_document(ET_object2)
from align_pages import *

#doc_data1.to_json_file('Dien_511_Duc.json')
#doc_data2.to_json_file('Dien_511_Viet.json')

#for item in doc_data1.iter_child_text():
  #print(item)

ordered_page1 = page_order(doc_data1[0], 2041, 2903, 2)
#for box in ordered_page1:
  #print(box.box)
  #print(box.text)
  #print(len(box.text))

ordered_page2 = page_order(doc_data2[0], 2006, 2832, 2)
#for box in ordered_page2:
  #print(box.box)
  #print(box.text)
  #print(len(box.text))


align_pair_page_to_csv(ordered_page1, ordered_page2, 'export_Dien_align.csv')

---
Batch align two documents:


from process_abbyy_xml import *
file1 = 'test/Dien_511_Duc.xml'
file2 = 'test/Dien_511_Viet.xml'
ET_object1 = clean_xml_read(file1)
doc_data1 = parse_abbyy_document(ET_object1)
ET_object2 = clean_xml_read(file2)
doc_data2 = parse_abbyy_document(ET_object2)
from align_pages import *

text_doc_1, text_doc_2 = align_pair_doc_to_csv(doc_data1, doc_data2, 'export_Dien_align.csv')

---
Export one XML file into a series of text files

from process_abbyy_xml import *
xmlfile = 'test/to_txt/Kunstoff_C01_T9.pdf.xml'
ET_object1 = clean_xml_read(xmlfile)
doc_data1 = parse_abbyy_document(ET_object1)
import align_pages

result = align_pages.parse_text_ocr_doc_to_txt(doc_data1, xmlfile, 2, 1)

"""
from PIL import Image, ImageDraw

from OcrData import OCR_Data
#import pdb

number_columns_default = 2 # page has 2 columns
length_threshold_default = 20 # filter out short texts

def pick_boxes(left_bound, right_bound, boxes):
    """Pick out boxes in a list that falls inside left_bound and right_bound
    """
    picked = set()
    for box_no in range(len(boxes)):
        if boxes[box_no][0] >= left_bound and boxes[box_no][2] <= right_bound:
            picked.add(box_no)
    return picked


def columnize_boxes(boxes, page_width, page_height, number_columns):
    """Sort boxes into columns.
    Input: list of coordinates of boxes in a page, page width and height, number of columns
    Coordinate: (left, top, right, bottom), with origin at top left corner of the page
    Output: ordered coordinates
    """
    #series_col = [list()] * number_columns # tested a bug: if not initialized,
    # then series_col[0].extend([1,2]) will add [1,2] to all other element of series_col!
    series_col = [None] * number_columns
    for t in range(number_columns):
        series_col[t] = []
    item_index = list(range(len(boxes)))
    offset_ = 0
    # Algorithm: search for boxes in the left most columns to the right most one.
    #   The width for each column is increased until it covers the whole page width.
    #   Algorithm stops if all boxes are sorted into columns.
    while len(item_index)>=1 and offset_<page_width: # boxes wider than page_width are discarded
        for col in range(number_columns):
            left_bound_col = page_width/number_columns*col - offset_
            right_bound_col = page_width/number_columns*(col+1) + offset_
            indices_picked = pick_boxes(left_bound_col, right_bound_col, [boxes[k] for k in item_index])
            series_col[col].extend([item_index[l] for l in indices_picked])
            for item_no in sorted(indices_picked, reverse=True):
                del item_index[item_no]
            # for quick debugging
            #print('col:',col)
            #print('item_index:',item_index)
            #print('series_col[0]:', series_col[0])
            #print('series_col[1]:', series_col[1])
        # increasing offset to enlarge each column for searching
        offset_ += 0.1*page_width/number_columns
   # return the set of indices of boxes that fall into each column
    return series_col


def order_boxes_per_column(boxes):
    """Order boxes in a column by top down, using the top position to sort
    """
    return sorted(boxes, key=lambda box: box[1])


def order_boxes(boxes, page_width, page_height, number_columns):
    """Order boxes in a page, first compare by column, then by top down position
    """
    sorted_columns = columnize_boxes(boxes, page_width, page_height, number_columns)
    order_col_boxes = [None] * number_columns
    for col in range(number_columns):
        boxes_col = [boxes[k] for k in sorted_columns[col]]
        order_col_boxes[col] = order_boxes_per_column(boxes_col)
    return order_col_boxes, sorted_columns

def order_blocks_per_column(blocks):
    return sorted(blocks, key=lambda block: block.box[1])

def order_blocks(blocks, page_width, page_height, number_columns):
    """Order blocks in a page, first compare by column, then by top down position.
    Input: series of OCR_Data, each item is a block
    Output: series of OCR_Data that are ordered
    """
    bounds_block = [block.box for block in blocks]
    sorted_columns = columnize_boxes(bounds_block, page_width, page_height, number_columns)
    order_col_blocks = [None] * number_columns
    for col in range(number_columns):
        blocks_col = [blocks[k] for k in sorted_columns[col]]
        order_col_blocks[col] = order_blocks_per_column(blocks_col)
    return order_col_blocks, sorted_columns

def page_order(page_data, page_width, page_height, number_columns):
    """Order children (blocks) in a page, first compare by column, then by top down position.
    Input: OCR_Data object of a page.
    Output: OCR_Data of the new OCR_Data object where its children are ordered.
    """

    page_in_cols, sorted_columns = order_blocks(page_data[:], page_width, page_height, number_columns)
    page_copy = page_data.copy() # to create a separate object with the same data
    page_copy[0:len(sorted_columns[0])] = page_in_cols[0]
    for prec_col in range(number_columns-1):
        page_copy[len(sorted_columns[prec_col]):len(sorted_columns[prec_col])+len(sorted_columns[prec_col+1])] = page_in_cols[prec_col+1]
    return page_copy

def page_order_auto(page_data, number_columns = number_columns_default):
    """Order children (blocks) in a page, first compare by column, then by top down position.
    Automatically detect width and height of the page, using page.attrib
    Input: OCR_Data object of a page.
    Output: OCR_Data of the new OCR_Data object where its children are ordered.
    """

    page_width = int(page_data.attrib['width'])
    page_height = int(page_data.attrib['height'])
    return page_order(page_data, page_width, page_height, number_columns)

def remove_short_pieces(blocks, length_threshold = length_threshold_default):
    """Remove pieces with text shorter than a threshold
    """
    for k in reversed(range(len(blocks))):
        if len(blocks[k].text) < length_threshold:
            del blocks[k]
    return blocks

def select_matching_box(box, series_boxes):
    """Select the closest box (in position) to a box in search
    box coordinate: tuple (left, top, right, bottom)
    series_boxes: a list of boxes
    """
    objective_ = [0] * len(series_boxes)
    for k in range(len(series_boxes)):
        #print(' Box 1: ',box)
        #print(' Box 2: ',series_boxes[k])
        objective_[k] = 2*(box[1] - series_boxes[k][1])**2 + (box[3] - series_boxes[k][3])**2 + 4*(box[0] - series_boxes[k][0])**2 + 4*(box[2] - series_boxes[k][2])**2
        #print('  Obj: ', objective_[k])

        # 2*(top - top_x)^2 + (bottom - bottom_x)^2 + 4*(left-left_x)^2 + 4*(right-right_x)^2
    # find the one with minimum objective_
    min_result = min(objective_)
    selected_index = objective_.index(min_result)
    print('   Minimum: ', min_result)
    return selected_index, min_result

def select_matching_box_except(box, series_boxes, fixed_untouch):
    """Select the closest box (in position) to a box in search
    box coordinate: tuple (left, top, right, bottom)
    series_boxes: a list of boxes
    fixed_untouch: indices of items that should not be selected (exceptions)
    """
    sorted_fixed = sorted(fixed_untouch)
    new_series_boxes = [series_boxes[k] for k in range(len(series_boxes)) if k not in sorted_fixed]
    print('sorted_fixed: ', sorted_fixed)
    print('new series boxes: ', new_series_boxes)
    selected_index, min_result = select_matching_box(box, new_series_boxes)
    for k in range(len(sorted_fixed)):
        if selected_index >= sorted_fixed[k]: selected_index += 1
    return selected_index, min_result

def compare_blocks(blocks1, blocks2):
    remaining_boxes2 = [block.box for block in blocks2.copy()]
    matching_series = []
    # matching_series to contain "pairs": [index of block in blocks1, index in blocks 2, distance value]
    # first iteration: rough estimate with possible overlaps
    for k in range(len(blocks1)):
        print('k: ', k)
        current_block = blocks1[k]
        current_matching_index, current_min_result = select_matching_box(current_block.box, remaining_boxes2)
        matching_series.append([k, current_matching_index, current_min_result])
    # improving iterations to remove overlaps
    # algorithm: the pair with lower min_result is fixed earlier
    matching_series = sorted(matching_series, key=lambda pair_: pair_[2])
    print('matching_series: ', matching_series)
    for k in range(len(matching_series)):
        fixed_untouch = [pair_[1] for pair_ in matching_series[0:k-1]]
        # TODO: error when the lengths of two series of blocks are not equal
        #       in that case at some point fixed_untouch is empty, select_matching_box_except will yield error
        if matching_series[k][1] in fixed_untouch:
            new_matching_index, new_min_result = select_matching_box_except(blocks1[matching_series[k][0]].box, remaining_boxes2, fixed_untouch)
            new_pair = [matching_series[k][0], new_matching_index, new_min_result]
            matching_series[k] = new_pair
    matching_series = sorted(matching_series, key=lambda pair_: pair_[0])
    return matching_series

def find_match(element, list_to_check):
    if not element in list_to_check:
        return None
    else:
        return list_to_check.index(element)

def extract_text_ocr_page(ocr_page, length_threshold = length_threshold_default):
    """Reflow text in an OCR page into the right order: sort by column from left to right,
    in each column: from top to bottom.
    Input: a sorted OCR page
    Output: combined text, reflown, with short text removed
    """
    filtered_page = remove_short_pieces(ocr_page.copy(), length_threshold)
    print('Removed texts shorter than the length threshold: ', length_threshold)
    print('Number of blocks in OCR page ', ocr_page, ': ', len(ocr_page))
    print('                 in filtered page: ', len(filtered_page))

    text_combine_list = [text_ for text_ in filtered_page.iter_child_text()]
    text_combine_string = '\r\n'.join(text_combine_list)
    #print('text_combine_string: ', text_combine_string)
    return text_combine_string

def parse_text_ocr_doc(ocr_doc, number_columns = number_columns_default, length_threshold = length_threshold_default):
    """Parse text in an OCR document into separate page-texts. Each page-text is reflown:
    sort by column from left to right, in each column: from top to bottom.
    Input: an OCR document
    Output: a list of strings, each item is reflown text in a page, ordered
    """
    text_doc_strings = []
    for k in range(len(ocr_doc)):
        if ocr_doc[k].tag == 'page':
            re_ordered_page = page_order_auto(ocr_doc[k], number_columns)
            text_doc_strings.append(extract_text_ocr_page(re_ordered_page, length_threshold))
    return text_doc_strings

def detect_page_filename(filePath):
    import re, os

    base_name = ''
    start_page_no = 1 # default is 1, if filePath has no special structure of page number
    dir_name = os.path.dirname(filePath)
    filename_base = os.path.basename(filePath)
    # recursively remove extentions: Kap_7 (310-369).pdf.xml
    # note: when there are dots in the main file name, like Kap_7_2017.0.31_p35.pdf.xml, then everything after first dot is wiped,
    # temporarily fixed by using condition len(sub_ext_name)<4. It could still have problem! Better check with text 'pdf', 'xml'...
    ext_name_set = {'.txt', '.pdf', '.xml', '.doc', '.docx', '.xls', '.xlsx'}
    main_name, ext_name = os.path.splitext(filename_base)
    base_name, sub_ext_name = os.path.splitext(main_name)
    while base_name != main_name and len(sub_ext_name)>0 and sub_ext_name.lower() in ext_name_set:
        main_name = base_name
        base_name, sub_ext_name = os.path.splitext(main_name)

    base_name = main_name

    patterns = [r'(.+)_(\d+)$', # Kap_14_515
                r'(.+)_p(\d+)$', # Kap_14_p515
                r'(.+)_P(\d+)$', # Kap_14_P515
                r'(.+)_t(\d+)$', # Kap_14_t515
                r'(.+)_tr(\d+)$', # Kap_14_tr515
                r'(.+)_T(\d+)$', # Kap_14_T515
                r'(.+)_Tr(\d+)$', # Kap_14_Tr515
                r'(.+)\((\d+)-\d+\)$', # Kap_14 (492-515)
                r'(.+)T(\d+)-\d+$', # C03 T073-107
                r'(.+)_(\d+)-\d+$', # C03 _073-107
                ]
    for patt in patterns:
        check_patt = re.search(patt, main_name)
        if check_patt:
            start_page_no = check_patt.group(2)
            base_name = check_patt.group(1)

    base_path = os.path.join(dir_name, base_name)
    #pdb.set_trace()
    return base_path, int(start_page_no)

def parse_text_ocr_doc_to_txt(ocr_doc, base_file_path, number_columns = number_columns_default, length_threshold = length_threshold_default):
    """Parse text in an OCR document into separate .txt files containing page-texts.
    Each page-text is reflown:
    sort by column from left to right, in each column: from top to bottom.
    Input: an OCR document
    Output: .txt files saved, list of file names
    """
    text_doc_strings = parse_text_ocr_doc(ocr_doc, number_columns, length_threshold)
    array_files = []
    base_path, start_page_no = detect_page_filename(base_file_path)
    for kk in range(len(text_doc_strings)):
        current_file = base_path + '_p' + str(kk + start_page_no) + '.txt'
        with open(current_file, 'wt', encoding='utf8') as text_file:
            text_file.write(text_doc_strings[kk])
        array_files.append(current_file)
    return array_files

def align_pair_page(ocr_page1, ocr_page2, length_threshold = length_threshold_default):
    """ Align texts within two translating correspondents of a page.
    Input: OCR_Data objects of two pages, each contained sorted blocks
    Output: Two strings of text reflown from two pages
    """

    #blocks1 = ocr_page1[:].copy()
    #blocks2 = ocr_page2[:].copy()
    #filtered_blocks1 = remove_short_pieces(blocks1, length_threshold)
    #filtered_blocks2 = remove_short_pieces(blocks2, length_threshold)
    #print('blocks1: ', len(blocks1))
    #print('filtered_blocks1: ',len(filtered_blocks1))
    #print('blocks2: ',len(blocks2))
    #print('filtered_blocks2: ', len(filtered_blocks2))
    #matching_series, _ = compare_blocks(filtered_blocks1, filtered_blocks2)
    #csv_table = csv_from_pairs(matching_series, filtered_blocks1, filtered_blocks2)

    text_1_string = extract_text_ocr_page(ocr_page1, length_threshold)
    text_2_string = extract_text_ocr_page(ocr_page2, length_threshold)
    #print('text_1_string: ', text_1_string)
    #print('text_2_string: ', text_2_string)
    # for now, only output the combined text, they should be aligned further
    return text_1_string, text_2_string

def align_pair_page_to_csv(ocr_page1, ocr_page2, csv_file, length_threshold = length_threshold_default):
    """ Align texts within two translating correspondents of a page.
    Input: OCR_Data objects of two pages, each contains sorted blocks
    Output: Two strings of text reflown from two pages, also save to a CSV file
    """
    import Csv_Table, Csv_Excel
    text_1_string, text_2_string = align_pair_page(ocr_page1, ocr_page2, csv_file, length_threshold)
    csv_table = []
    csv_table.append([text_1_string, text_2_string])
    Csv_Table.write_table_csv(csv_file, csv_table)
    xls_file = csv_file.replace('.csv', '.xls')
    Csv_Excel.csv_to_xls(csv_file, xls_file)
    return text_1_string, text_2_string

def align_pair_doc(ocr_doc1, ocr_doc2, number_columns = number_columns_default, length_threshold = length_threshold_default):
    """ Align texts within two documents, put text in a CSV, page by page.
    Input: OCR_Data objects of two documents, both documents contain the same number of pages
    Output: Two lists of strings of text reflown from pages
    """
    print('Aligning two documents. Requirement: two documents must have the same pages.')
    text_doc_1_strings = []
    text_doc_2_strings = []
    for k in range(len(ocr_doc1)):
        if ocr_doc1[k].tag == 'page':
            re_ordered_page1 = page_order_auto(ocr_doc1[k], number_columns)
            text_doc_1_strings.append(extract_text_ocr_page(re_ordered_page1, length_threshold))
        if ocr_doc2[k].tag == 'page':
            re_ordered_page2 = page_order_auto(ocr_doc2[k], number_columns)
            text_doc_2_strings.append(extract_text_ocr_page(re_ordered_page2, length_threshold))
    return text_doc_1_strings, text_doc_2_strings

def align_pair_doc_to_csv(ocr_doc1, ocr_doc2, csv_file, number_columns = number_columns_default, length_threshold = length_threshold_default):
    """ Align texts within two documents, put text in a CSV, page by page.
    Input: OCR_Data objects of two documents, both documents contain the same number of pages
    Output: Two lists of strings of text reflown from pages, also save to a CSV file
    """
    import Csv_Table, Csv_Excel
    text_doc_1_strings, text_doc_2_strings = align_pair_doc(ocr_doc1, ocr_doc2, number_columns, length_threshold)
    csv_table = []
    for k in range(len(text_doc_1_strings)):
        csv_table.append([text_doc_1_strings[k], text_doc_2_strings[k]])
    Csv_Table.write_table_csv(csv_file, csv_table)
    xls_file = csv_file.replace('.csv', '.xls')
    Csv_Excel.csv_to_xls(csv_file, xls_file)
    return text_doc_1_strings, text_doc_2_strings


def draw_boxes(image, bounds, color):
    """Draw a border around the image using the coordinates stored in bounds."""
    draw = ImageDraw.Draw(image)

    for bound in bounds:
        draw.polygon([
            bound[0], bound[1], #top left
            bound[2], bound[1], #top right
            bound[2], bound[3], #bottom right
            bound[0], bound[3]], None, color) #bottom left
    return image


def render_doc_text(filein, ocr_data_in, fileout, json_fileout):

    image = Image.open(filein)

    #print('All recognized text in the image: \n{}'.format(full_text))

    if json_fileout is not 0:
        ocr_data_in.to_json_file(json_fileout)


    bounds_block = [block.box for block in ocr_data_in.iter('block')]
    bounds_paragraph = [para.box for para in ocr_data_in.iter('paragraph')]
    bounds_word = [word.box for word in ocr_data_in.iter('word')]

    draw_boxes(image, bounds_block, 'blue')
    draw_boxes(image, bounds_paragraph, 'red')
    draw_boxes(image, bounds_word, 'yellow')

    if fileout is not 0:
        image.save(fileout)
    else:
        image.show()

def csv_from_pairs(matching_series, blocks1, blocks2):
    table_ = []
    for pair_ in matching_series:
        blk1 = blocks1[pair_[0]]
        blk2 = blocks2[pair_[1]]
        table_.append([blk1.text, blk2.text, blk1.box, blk2.box])
    return table_
