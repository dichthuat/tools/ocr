#!/usr/bin/env python

# Written 2019 Dang Doan, adopting examples by Google.
#
# Licensed under the MIT license.
"""Call Google Cloud Vision for doing OCR, and process the output data.
Additionally, this code outlines document text given an image.

Example:
    python3 doctext_detect_gcloud.py test/test_chem_nnt.png -out_file test/bounds_test_chem_nnt.png -json_file test/test_chem_nnt.json
"""
# [START vision_document_text_tutorial_imports]
import argparse
from enum import Enum
import io

from google.cloud import vision
from google.cloud.vision import types
from PIL import Image, ImageDraw
# [END vision_document_text_tutorial_imports]

import json
from OcrData import OCR_Data

def gcloud_bound_to_box(bound):
    left = min(int(vertice.x) for vertice in bound.vertices)
    right = max(int(vertice.x) for vertice in bound.vertices)
    top = min(int(vertice.y) for vertice in bound.vertices)
    down = max(int(vertice.y) for vertice in bound.vertices)
    box = (left, top, right, down)
    return box

def draw_boxes(image, bounds, color):
    """Draw a border around the image using the coordinates stored in bounds."""
    draw = ImageDraw.Draw(image)

    for bound in bounds:
        draw.polygon([
            bound[0], bound[1], #top left
            bound[2], bound[1], #top right
            bound[2], bound[3], #bottom right
            bound[0], bound[3]], None, color) #bottom left
    return image

def append_json(json_object, text):
    """Append new text to a json string"""
    
def text_post_process(text):
    """Post process sentences which have spaces due to connecting words with space, like this:
    What ' s going on ? Visit www . my - website . com for explanations .
    """

def detect_document(path):
    """Detects document features in an image."""
    #from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image) # this command call Google API to do OCR, costly

    # analyze the text, save to a tree of OCR_Data objects
    current_document = OCR_Data() # initiate OCR_Data for adding its children iteratively
    
    for page in response.full_text_annotation.pages:
        
        current_page = OCR_Data()
        
        for block in page.blocks:

            current_block = OCR_Data()
            
            for paragraph in block.paragraphs:

                current_paragraph = OCR_Data()
                
                for word in paragraph.words:

                    current_word = OCR_Data()

                    for symbol in word.symbols:
                        current_word.append(OCR_Data(symbol.text, symbol.confidence, gcloud_bound_to_box(symbol.bounding_box), 'symbol'))
                        print('\tSymbol: {} (confidence: {})'.format(
                            symbol.text, symbol.confidence))

                    word_text = ''.join([symbol.text for symbol in word.symbols])
                    current_word.text = word_text
                    current_word.confidence = word.confidence
                    current_word.box = gcloud_bound_to_box(word.bounding_box)
                    current_word.tag = 'word'
                    current_paragraph.append(current_word)
                    print('Word text: {} (confidence: {})'.format(
                        word_text, word.confidence))

                paragraph_text = ' '.join(word_data.text for word_data in current_paragraph)
                current_paragraph.text = paragraph_text
                current_paragraph.confidence = paragraph.confidence
                current_paragraph.box = gcloud_bound_to_box(paragraph.bounding_box)
                current_paragraph.tag = 'paragraph'
                current_block.append(current_paragraph)
                print('Paragraph text: {} (confidence: {})\n'.format(paragraph_text, paragraph.confidence))

            block_text = '\n'.join(paragraph_data.text for paragraph_data in current_block)
            current_block.text = block_text
            current_block.confidence = block.confidence
            current_block.box = gcloud_bound_to_box(block.bounding_box)
            print(block.bounding_box)
            print(gcloud_bound_to_box(block.bounding_box))
            current_block.tag = 'block'
            current_page.append(current_block)
            print('\nBlock text: {} (confidence: {})\n'.format(block_text, block.confidence))

        page_text = '\n\n'.join(block_data.text for block_data in current_page)
        current_page.text = page_text
        current_page.confidence = page.confidence
        current_page.box = (-1, -1, -1, -1)
        current_page.tag = 'page'
        current_document.append(current_page)
        print('\nPage text: {} (confidence: {})\n'.format(page_text, page.confidence))

    all_document_text = '\n\n'.join(page_data.text for page_data in current_document)
    current_document.text = all_document_text
    current_document.confidence = 0.0
    current_document.box = (-1, -1, -1, -1)
    current_document.tag = 'document'

    document = response.full_text_annotation
    full_text = document.text

    return current_document, full_text

def render_doc_text(filein, fileout, json_fileout):
    image = Image.open(filein)
    current_document, full_text = detect_document(filein)

    print('All recognized text in the image: \n{}'.format(full_text))

    if json_fileout is not 0:
        current_document.to_json_file(json_fileout)


    # for testing, use json conversion
    #all_document_json = current_document.to_json_string()
    #print(all_document_json['page'][0]['text'])
    #print(all_document_json['page'][0]['block'][0]['text'])
    #print(all_document_json['page'][0]['block'][0]['paragraph'][0]['text'])
    #print(all_document_json['page'][0]['block'][0]['paragraph'][0]['word'][1]['text'])
    
    # or taking data directly
    #print(current_document[0].text) # text of a page
    #print(current_document[0][0].text) # text of a block
    #print(current_document[0][0][0].text) # text of a paragraph
    #print(current_document[0][0][0][0].text) # text of a word
    #print(current_document[0][0][0][0][0].text) # text of a symbol

    bounds_block = [block.box for block in current_document.iter('block')]
    bounds_paragraph = [para.box for para in current_document.iter('paragraph')]
    bounds_word = [word.box for word in current_document.iter('word')]

    draw_boxes(image, bounds_block, 'blue')
    draw_boxes(image, bounds_paragraph, 'red')
    draw_boxes(image, bounds_word, 'yellow')

    if fileout is not 0:
        image.save(fileout)
    else:
        image.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('detect_file', help='The image for text detection.')
    parser.add_argument('-out_file', help='Optional output image file', default=0)
    parser.add_argument('-json_file', help='Optional output json file', default=0)
    args = parser.parse_args()

    render_doc_text(args.detect_file, args.out_file, args.json_file)
