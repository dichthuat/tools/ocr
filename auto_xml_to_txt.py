#!/usr/bin/env python
# Usage:
# python3 auto_xml_to_txt xml_path
# with the xml_path can be a .xml file, or a folder containing XML files

import os, sys
import process_abbyy_xml
import align_pages

def auto_xml_file_to_txt(xmlfile):
    #xmlfile = 'test/to_txt/Kunstoff_C01_T9.pdf.xml'
    ET_object1 = process_abbyy_xml.clean_xml_read(xmlfile)
    doc_data1 = process_abbyy_xml.parse_abbyy_document(ET_object1)
    result = align_pages.parse_text_ocr_doc_to_txt(doc_data1, xmlfile)
    return result

def auto_xml_folder_to_txt(folder_with_xml):
    import glob
    xml_paths = glob.glob(folder_with_xml + '/*.xml')
    result = []
    for xmlfile in xml_paths:
        result.extend(auto_xml_file_to_txt(xmlfile))
    return result

if __name__ == '__main__':
    path_param = str(sys.argv[1])
    if os.path.isdir(path_param):
        result = auto_xml_folder_to_txt(path_param)
    elif os.path.isfile(path_param):
        result = auto_xml_file_to_txt(path_param)
    print("These new files have been created: \n", result)
