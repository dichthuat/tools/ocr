// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// This sample shows basic steps of ABBYY FineReader Engine usage:
// Initializing, opening image file, recognition and export.

#include "FREngineLoader.h"
#include "BstrWrap.h"
#include "AbbyyException.h"
#include "../SamplesConfig.h"

// Forward declaration
void displayMessage( const wchar_t* text );
void processImage();
void setupFREngine();

void executeTask()
{
	try {			

		// Load ABBYY FineReader Engine
		displayMessage( L"Initializing Engine..." );
		LoadFREngine();
        displayMessage( L"Seting up Engine..." );
        setupFREngine();

		// Process Demo.tif image in SampleImages folder
		processImage();

		// Unload ABBYY FineReader Engine
		displayMessage( L"Deinitializing Engine..." );
		UnloadFREngine();

	} catch( CAbbyyException& e ) {
		displayMessage( e.Description() );
	}
}


void setupFREngine()
{
	// This function loads predefined profile for FineReader Engine
	displayMessage( L"Loading predefined profile for Engine..." );
	CheckResult( FREngine->LoadPredefinedProfile( CBstr( L"TextExtraction_Accuracy" ) ) );
    // Load user profile
    CheckResult( FREngine->LoadProfile( CBstr( L"./dang_pdf_xml.ini" ) ) );
}


void processImage()
{	
	// Create document from image file
	displayMessage( L"Loading image..." );
	CBstr imagePath = Concatenate( GetSamplesFolder(), L"/SampleImages/Dien_511-513_Duc.pdf" );
	CSafePtr<IFRDocument> frDocument = 0;
	CheckResult( FREngine->CreateFRDocumentFromImage( imagePath, 0, frDocument.GetBuffer() ) );

	//Recognize document
	displayMessage( L"Recognizing..." );
	CheckResult( frDocument->Process() );
// 		CSafePtr<IDocumentProcessingParams> documentProcessingParams;
// 		CheckResult( FREngine->CreateDocumentProcessingParams( &documentProcessingParams ) );
// 		CSafePtr<IPageProcessingParams> pageProcessingParams;
// 		CheckResult( documentProcessingParams->get_PageProcessingParams( &pageProcessingParams ) );
// 		CSafePtr<IRecognizerParams> recognizerParams;
// 		CheckResult( pageProcessingParams->get_RecognizerParams( &recognizerParams ) );
// 
// 		CheckResult( recognizerParams->SetPredefinedTextLanguage( CBstr( L"Vietnamese" ) ) );
// 
// 		CheckResult( frDocument->Process( documentProcessingParams ) );

	// Save results
	displayMessage( L"Saving results..." );
	CBstr exportPath = Concatenate( GetSamplesFolder(), L"/SampleImages/Dien_511-513_Duc_offline.xml" );
    
	CheckResult( frDocument->Export(  exportPath, FEF_XML, 0  ) );
}

void displayMessage( const wchar_t* text )
{
	wprintf( text );
	wprintf( L"\n" );
}

int main()
{
	executeTask();
	return 0;
}

